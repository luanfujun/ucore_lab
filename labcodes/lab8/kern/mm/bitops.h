// @file: kern/mm/bitops.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-04-30

#ifndef _KERN_MM_BITOPS_H
#define _KERN_MM_BITOPS_H
#pragma once

// find last set bit in word
static __always_inline 
size_t ufls(size_t word)
{
	asm("bsr %1,%0"
	    : "=r" (word)
	    : "rm" (word));
	return word + 1;
}

static inline
int fls(int x)
{
	int r = 0;
	asm("bsrl %1,%0\n\t"
	    "jnz 1f\n\t"
	    "movl $-1,%0\n"
	    "1:" : "=r" (r) : "rm" (x));
	return r + 1;
}

#endif // _KERN_MM_BITOPS_H
