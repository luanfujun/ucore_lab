// @file: kern/mm/swap_clock.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-05-02

#ifndef _KERN_MM_SWAPCLOCK_H
#define _KERN_MM_SWAPCLOCK_H
#pragma once

#include <swap.h>
#include <config.h>

#ifdef USE_MM_EXT
	extern struct swap_manager swap_manager_clock;
#endif  // USE_MM_EXT

#endif // _KERN_MM_SWAPCLOCK_H
