#include <stdio.h>
#include <monitor.h>
#include <kmalloc.h>
#include <assert.h>
#include <sync.h>

// Initialize monitor.
void     
monitor_init (monitor_t * mtp, size_t num_cv) {
    int i;
    assert(num_cv>0);
    mtp->next_count = 0;
    mtp->cv = NULL;
    sem_init(&(mtp->mutex), 1); //unlocked
    sem_init(&(mtp->next), 0);
    mtp->cv =(condvar_t *) kmalloc(sizeof(condvar_t)*num_cv);
    assert(mtp->cv!=NULL);
    for(i=0; i<num_cv; i++){
        mtp->cv[i].count=0;
        sem_init(&(mtp->cv[i].sem),0);
        mtp->cv[i].owner=mtp;
    }
}

// Unlock one of threads waiting on the condition variable. 
void 
cond_signal (condvar_t *cvp) {
  //LAB7 EXERCISE1: 2011011290
  cprintf("cond_signal begin: cvp %x, cvp->count %d, cvp->owner->next_count %d\n", cvp, cvp->count, cvp->owner->next_count);  
  bool intr_flags;
  local_intr_save(intr_flags);
    // To acknowledge the function of this method, see following.
    if (cvp->count > 0) {
      ++cvp->owner->next_count;
      up(&(cvp->sem));
      down(&(cvp->owner->next));
      --cvp->owner->next_count;
    }
  local_intr_restore(intr_flags);
  cprintf("cond_signal end: cvp %x, cvp->count %d, cvp->owner->next_count %d\n", cvp, cvp->count, cvp->owner->next_count);
}

// Suspend calling thread on a condition variable waiting for condition Atomically unlocks 
// mutex and suspends calling thread on conditional variable after waking up locks mutex. Notice: mp is mutex semaphore for monitor's procedures
void
cond_wait (condvar_t *cvp) {
  //LAB7 EXERCISE1: 2011011290
  cprintf("cond_wait begin:  cvp %x, cvp->count %d, cvp->owner->next_count %d\n", cvp, cvp->count, cvp->owner->next_count);
  bool intr_flags;
  local_intr_save(intr_flags);
    ++cvp->count;
    // Why up(next) here if next_count > 0?  The core ideal of monitor 
    // is to release the monitor's mutex on cond_wait.  This is always done 
    // in cond_wait if no signal has been sent.  
    // 
    // However, if two threads cond_wait at a same time, and when one 
    // cond_signal is emitted, it would immediately wakeup one waited 
    // thread, but the owner of monitor's mutex is now on signal thread. 
    //
    // To guarantee that signal thread should exit only after wait thread 
    // has been done, it should wait on monitor's next. So in this case, 
    // it's not wait thread's responsibility to release mutex, but signal's.
    //
    // The final release of the monitor's mutex is done by the caller.
    // Whatever, this composing style is so bad that maybe it's the 
    // reason why industrial operating systems do not require signal 
    // thread to wait until wait thread has finished.
    if (cvp->owner->next_count > 0) {
      up(&(cvp->owner->next));
    } else {
      up(&(cvp->owner->mutex));
    }
    down(&(cvp->sem));
    --cvp->count;
  local_intr_restore(intr_flags);
  cprintf("cond_wait end:  cvp %x, cvp->count %d, cvp->owner->next_count %d\n", cvp, cvp->count, cvp->owner->next_count);
}
