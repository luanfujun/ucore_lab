#ifndef __KERN_MM_SWAP_FIFO_H__
#define __KERN_MM_SWAP_FIFO_H__

#include <swap.h>
#include <config.h>

#ifndef USE_MM_EXT
	extern struct swap_manager swap_manager_fifo;
#endif  // USE_MM_EXT

#endif
