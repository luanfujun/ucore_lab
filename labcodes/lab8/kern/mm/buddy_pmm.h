// @file: kern/mm/buddy_pmm.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-04-19

#ifndef _KERN_MM_BUDDYPMM_H
#define _KERN_MM_BUDDYPMM_H
#pragma once

#include <pmm.h>

extern const struct pmm_manager buddy_pmm_manager;

// Sometimes it is necessary to backup pmm states for test purpose.
void buddy_backup_states();
void buddy_restore_states();

#endif // _KERN_MM_BUDDYPMM_H
