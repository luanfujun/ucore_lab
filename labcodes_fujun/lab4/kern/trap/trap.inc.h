// @file: study/2013.02/OS/homework/ucore_lab/code/lab3/kern/trap/trap.inc.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-04-08

#ifndef _STUDY_2013_02_OS_HOMEWORK_UCORELAB_CODE_LAB3_KERN_TRAP_TRAP_INC_H
#define _STUDY_2013_02_OS_HOMEWORK_UCORELAB_CODE_LAB3_KERN_TRAP_TRAP_INC_H
#pragma once

/* Code for LAB1 : STEP 3 */
static void
trap_dispatch_timer(struct trapframe *tf) {
  static volatile size_t counter = 0;
  
  // Print message for every 100 ticks.
  if (++counter == TICK_NUM) {
    counter = 0;
    print_ticks();
  }
  
  // Increase the internal tick counter.
  ++ticks;
}

/* trap_disp_ktou - switch from kernel mode to user mode */
static void
trap_disp_ktou(struct trapframe *tf) {
  // According to some online documents, the instruction "iret" would pop 
  // cs:eip & ss:esp from the stack. To switch between kernel and user mode, 
  // values in stack should be overwritten.
  // (See:
  //  http://stackoverflow.com/questions/6892421/switching-to-user-mode-using-iret
  //  http://www.jamesmolloy.co.uk/tutorial_html/10.-User%20Mode.html
  // )
  //
  // However, uCore maps all the necessary registers into the stack, and provide 
  // the trapframe interface as C struct. So it is much easier to do the tricks.
  
  if (tf->tf_cs != USER_CS) {
    // Convert the segment registers to user mode.
    tf->tf_cs = USER_CS;
    tf->tf_ds = tf->tf_es = tf->tf_fs = tf->tf_gs = tf->tf_ss = USER_DS;
    
    // Enable IO operations under user mode.
    tf->tf_eflags |= FL_IOPL_MASK;
  }
}

/* trap_disp_utok - switch from user mode to kernel mode */
static void
trap_disp_utok(struct trapframe *tf) {
  if (tf->tf_cs != KERNEL_CS) {
    // Convert the segment registers to user mode.
    tf->tf_cs = KERNEL_CS;
    tf->tf_ds = tf->tf_es = tf->tf_fs = tf->tf_gs = tf->tf_ss = KERNEL_DS;
    
    // Remove IO operation privileges.
    tf->tf_eflags &= ~FL_IOPL_MASK;
  }
}

/* trap_disp_syscall - execute the syscall function. */
static void
trap_disp_syscall(struct trapframe *tf) {
  tf->tf_regs.reg_eax = 0xdeadbeef;
}


#endif // _STUDY_2013_02_OS_HOMEWORK_UCORELAB_CODE_LAB3_KERN_TRAP_TRAP_INC_H
