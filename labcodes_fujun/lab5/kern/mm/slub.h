// @file: kern/mm/slub.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-04-30

#ifndef _KERN_MM_SLUB_H
#define _KERN_MM_SLUB_H
#pragma once

#include <defs.h>

void kmalloc_init(void);
void *kmalloc(size_t n);
void kfree(void *objp);

#endif // _KERN_MM_SLUB_H
