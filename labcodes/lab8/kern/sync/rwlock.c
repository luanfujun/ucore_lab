// @file: kern/sync/rwlock.cc
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-06-03

#include <rwlock.h>
#include <string.h>
#include <proc.h>
#include <stdio.h>
#include <assert.h>

// ---- Implementation of Reader Prefered rwlock ----
static bool rwlock_init_pr(rwlock_t *self)
{
  self->type = RWLOCK_READER_PREFERRED;
  self->d.pr.active_reader = 0;
  sem_init(&(self->d.pr.rw_mutex), 1);
  sem_init(&(self->d.pr.rc_mutex), 1);
  return 1;
}

static void rwlock_destroy_pr(rwlock_t *self)
{
  // TODO: wait for all readers and writers to exit.
  memset(self, 0, sizeof(rwlock_t));
}

static void rwlock_lock_read_pr(rwlock_t *self)
{
  down(&(self->d.pr.rc_mutex));
    if (++self->d.pr.active_reader == 1) {
      // I tried to avoid rc_mutex by atomic operations but failed.
      //
      // Let's exam the following circumstances:
      // 1. ++counter = 2 -> --counter = 1: rw_mutex is not released
      // 2. --counter = 0 -> ++counter = 1 -> up mutex -> down mutex:
      //    Writer may grab the resource between up & down
      // 3. --counter = 0 -> ++counter = 1 -> down mutex -> up mutex:
      //    If writer is in the wait queue, then it will kick reader out 
      //    and become the first to be awaken by unlock_read.
      //
      // All the three cases will not break that "triple read, one write".
      // But it does not satisfy "Writer must wait for all readers."  
      // So avoid rc_mutex is not possible.
      down(&(self->d.pr.rw_mutex));
    }
  up(&(self->d.pr.rc_mutex));
}

static void rwlock_unlock_read_pr(rwlock_t *self)
{
  down(&(self->d.pr.rc_mutex));
    if (--self->d.pr.active_reader == 0) {
      up(&(self->d.pr.rw_mutex));
    }
  up(&(self->d.pr.rc_mutex));
}

static void rwlock_lock_write_pr(rwlock_t *self)
{
  down(&(self->d.pr.rw_mutex));
}

static void rwlock_unlock_write_pr(rwlock_t *self)
{
  up(&(self->d.pr.rw_mutex));
}

// ---- Implementation of Writer Prefered rwlock ----
static bool rwlock_init_pw(rwlock_t *self)
{
  self->type = RWLOCK_WRITER_PREFERRED;
  self->d.pw.active_reader = self->d.pw.active_writer = 0;
  sem_init(&(self->d.pw.sem_rc), 1);
  sem_init(&(self->d.pw.sem_wc), 1);
  sem_init(&(self->d.pw.sem_noreader), 1);
  sem_init(&(self->d.pw.sem_nowriter), 1);
  sem_init(&(self->d.pw.sem_waiting_reader), 1);
  return 1;
}

static void rwlock_destroy_pw(rwlock_t *self)
{
  // TODO: wait for all readers and writers to exit.
  memset(self, 0, sizeof(rwlock_t));
}

static void rwlock_lock_read_pw(rwlock_t *self)
{
  down(&(self->d.pw.sem_waiting_reader));
    // When writer is in the wait queue (so it is waiting for active reader),
    // first following reader is blocked by sem_nowriter, and other following 
    // readers are blocked by sem_waiting_reader, which is held by the first 
    // waiting reader.
    down(&(self->d.pw.sem_nowriter));
      down(&(self->d.pw.sem_rc));
        if (++self->d.pw.active_reader == 1)
          down(&(self->d.pw.sem_noreader));  // Forbids writer to enter.
      up(&(self->d.pw.sem_rc));
    up(&(self->d.pw.sem_nowriter));
  up(&(self->d.pw.sem_waiting_reader));
}

static void rwlock_unlock_read_pw(rwlock_t *self)
{
  down(&(self->d.pw.sem_rc));
    if (--self->d.pw.active_reader == 0)
      up(&(self->d.pw.sem_noreader));
  up(&(self->d.pw.sem_rc));
}

static void rwlock_lock_write_pw(rwlock_t *self)
{
  down(&(self->d.pw.sem_wc));
    if (++self->d.pw.active_writer == 1)
      down(&(self->d.pw.sem_nowriter));  // Forbids reader to enter.
  up(&(self->d.pw.sem_wc));
  down(&(self->d.pw.sem_noreader));
}

static void rwlock_unlock_write_pw(rwlock_t *self)
{
  up(&(self->d.pw.sem_noreader));
  down(&(self->d.pw.sem_wc));
    if (--self->d.pw.active_writer == 1)
      up(&(self->d.pw.sem_nowriter));
  up(&(self->d.pw.sem_wc));
}

// ---- Interface of rwlock ----
bool rwlock_init(rwlock_t *self, rwlock_type type)
{
  switch (type)
  {
  case RWLOCK_READER_PREFERRED:
    return rwlock_init_pr(self);
  case RWLOCK_WRITER_PREFERRED:
    return rwlock_init_pw(self);
  default:
    return 0;
  }
}

void rwlock_destroy(rwlock_t *self)
{
  switch (self->type)
  {
  case RWLOCK_READER_PREFERRED:
    rwlock_destroy_pr(self);
    break;
  case RWLOCK_WRITER_PREFERRED:
    rwlock_destroy_pw(self);
    break;
  }
}

void rwlock_lock_read(rwlock_t *self)
{
  switch (self->type)
  {
  case RWLOCK_READER_PREFERRED:
    rwlock_lock_read_pr(self);
    break;
  case RWLOCK_WRITER_PREFERRED:
    rwlock_lock_read_pw(self);
    break;
  }
}

void rwlock_unlock_read(rwlock_t *self)
{
  switch (self->type)
  {
  case RWLOCK_READER_PREFERRED:
    rwlock_unlock_read_pr(self);
    break;
  case RWLOCK_WRITER_PREFERRED:
    rwlock_unlock_read_pw(self);
    break;
  }
}

void rwlock_lock_write(rwlock_t *self)
{
  switch (self->type)
  {
  case RWLOCK_READER_PREFERRED:
    rwlock_lock_write_pr(self);
    break;
  case RWLOCK_WRITER_PREFERRED:
    rwlock_lock_write_pw(self);
    break;
  }
}

void rwlock_unlock_write(rwlock_t *self)
{
  switch (self->type)
  {
  case RWLOCK_READER_PREFERRED:
    rwlock_unlock_write_pr(self);
    break;
  case RWLOCK_WRITER_PREFERRED:
    rwlock_unlock_write_pw(self);
    break;
  }
}

// ---- Check the functionality of rwlock ----
#define N 10
#define TIMES 5
#define SLEEP_TIME 5

typedef struct 
{
  int name;
  rwlock_t *lock;
} 
rwlock_check_argument_t;

static rwlock_t pr, pw;
static rwlock_check_argument_t 
  pr_read_arg[N/2],
  pr_write_arg[N/2],
  pw_read_arg[N/2],
  pw_write_arg[N/2];

static void rwlock_check_report_state(rwlock_t *lock, int name, bool is_reader, int loop)
{
  const char* reader_or_writer = (is_reader) ? ("reader") : ("writer");
  if (lock->type == RWLOCK_READER_PREFERRED) {
    if (loop >= 0) {
      cprintf("[rwlock] PR #%d %s loop %d, active_reader %d.\n", 
        name, reader_or_writer, loop, lock->d.pr.active_reader);
    } else {
      cprintf("[rwlock] PR #%d %s exit, active_reader %d.\n", 
        name, reader_or_writer, lock->d.pr.active_reader);
    }
  } else {
    if (loop >= 0) {
      cprintf("[rwlock] PW #%d %s loop %d, active_reader %d, active_writer %d.\n", 
        name, reader_or_writer, loop, lock->d.pw.active_reader, lock->d.pw.active_writer);
    } else {
      cprintf("[rwlock] PW #%d %s exit, active_reader %d, active_writer %d.\n", 
        name, reader_or_writer, lock->d.pw.active_reader, lock->d.pw.active_writer);
    }
  }
}

static int rwlock_check_read(void* arg)
{
  rwlock_check_argument_t *a = (rwlock_check_argument_t*)arg;
  int iter = 0;
  for (iter = 0; iter < TIMES; iter++) {
    rwlock_lock_read(a->lock);
    do_sleep(SLEEP_TIME);
    rwlock_check_report_state(a->lock, a->name, 1, iter);
    rwlock_unlock_read(a->lock);
  }
  rwlock_check_report_state(a->lock, a->name, 1, -1);
  return 0;
}

static int rwlock_check_write(void *arg)
{
  rwlock_check_argument_t *a = (rwlock_check_argument_t*)arg;
  int iter = 0;
  for (iter = 0; iter < TIMES; iter++) {
    rwlock_lock_write(a->lock);
    do_sleep(SLEEP_TIME);
    rwlock_check_report_state(a->lock, a->name, 0, iter);
    rwlock_unlock_write(a->lock);
  }
  rwlock_check_report_state(a->lock, a->name, 0, -1);
  return 0;
}

void rwlock_check()
{
  int i, pid;

  // Initialize the rwlock and arguments
  rwlock_init(&pr, RWLOCK_READER_PREFERRED);
  rwlock_init(&pw, RWLOCK_WRITER_PREFERRED);

  for (i=0; i<N/2; ++i) {
    pr_read_arg[i].name = i;
    pr_read_arg[i].lock = &pr;
    pr_write_arg[i].name = i;
    pr_write_arg[i].lock = &pr;
    pw_read_arg[i].name = i;
    pw_read_arg[i].lock = &pw;
    pw_write_arg[i].name = i;
    pw_write_arg[i].lock = &pw;
  }

#if 0
  // Check reader prefered rwlock.
  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_check_read, &(pr_read_arg[i]), 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_check_read_pr");
  }

  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_check_write, &(pr_write_arg[i]), 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_check_write_pr");
  }

  // Check writer prefered rwlock.
  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_check_read, &(pw_read_arg[i]), 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_check_read_pw");
  }

  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_check_write, &(pw_write_arg[i]), 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_check_write_pw");
  }
#endif

  // Check condvar
  extern void rwlock_check_condvar();
  rwlock_check_condvar();
}


