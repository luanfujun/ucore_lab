// @file: kern/sync/pwx_condvar.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-06-04

#ifndef _KERN_SYNC_PWXCONDVAR_H
#define _KERN_SYNC_PWXCONDVAR_H
#pragma once

#include <sem.h>

/**
 * @brief A simplified version of monitor-condvar matching pthread interface.
 * 
 * A monitor carries multiple condition variables, while signal to a certain 
 * variable requires the waiting thread to be finished.  However, in most 
 * cases the signal thread need not wait for the waiting thread.
 */
typedef struct {
  volatile int count;    // Waiting threads on this condvar.
  semaphore_t sem_wait;  // Waiting semaphore.
  semaphore_t sem_count; // Semaphore to guard the access of count.
} 
pwx_condvar_t;

/**
 * @brief Initialize a condvar instance.
 * @param self: The condvar instance to be initialized.
 */
void pwx_condvar_init(pwx_condvar_t *self);

/**
 * @brief Wait on a condvar.
 * @param self: The condvar instance to wait.
 * @param mutex: The related monitor mutex. 
 *
 * The monitor mutex will be released before waiting for condvar, and 
 * will be re-grabbed after signal emitted.
 */
void pwx_condvar_wait(pwx_condvar_t *self, semaphore_t *mutex);

/**
 * @brief Signal a condvar.
 * @param self: The condvar instance to signal.
 *
 * To wakeup all waiting threads, use condvar_broadcast instead.  Related 
 * mutex must be grabbed before this method is called.
 */
void pwx_condvar_signal(pwx_condvar_t *self);

/**
 * @brief Broadcast a condvar.
 * @param self: The condvar instance to broadcast.
 *
 * Wakeup all waiting threads. Related mutex must be grabbed before this 
 * method is called.
 */
void pwx_condvar_signal(pwx_condvar_t *self);

#endif // _KERN_SYNC_PWXCONDVAR_H
