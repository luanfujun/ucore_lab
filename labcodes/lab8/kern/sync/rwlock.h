// @file: kern/sync/rwlock.h
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-06-03

#ifndef _KERN_SYNC_RWLOCK_H
#define _KERN_SYNC_RWLOCK_H
#pragma once

#include <sem.h>

//#define ENABLE_TEST_RWLOCK 1

/**
 * @brief Privilege type of an rwlock.
 * 
 * When rwlock prefers reader, any write request must wait for all readers
 * to be done, including the readers which request after the writer.  When 
 * rwlock prefers writer, it does the opposite.
 */
typedef enum 
{
  RWLOCK_READER_PREFERRED = 1,  // Indicate that the rwlock prefers reader.
  RWLOCK_WRITER_PREFERRED = 2,  // Indicate that the rwlock prefers writer.
} rwlock_type;

/**
 * @brief The wrapper for all rwlock types.
 */
typedef struct
{
  rwlock_type type;
  union {
    // The implementation data for reader prefered rwlock.
    struct {
      // Count of active readers.
      volatile int active_reader;
      // Semaphore to guard the modify of active_reader count.
      semaphore_t rc_mutex;
      // Semaphore to indicate whether an active writer or a group of 
      // active readers are existed.  This semaphore is essential, 
      // in that it can put reader and writer into system wait queue.
      semaphore_t rw_mutex;
    } pr;

    // The implementation data for writer prefered rwlock.
    struct {
      // Count of active readers.
      volatile int active_reader;
      // Count of active writers.
      volatile int active_writer;
      // Semaphore to guard active_reader and active_writer;
      semaphore_t sem_rc, sem_wc;
      // Semaphore to signal that "there's no active reader".
      semaphore_t sem_noreader;
      // Semaphore to signal that "there's no active writer".
      semaphore_t sem_nowriter;
      // Semaphore to signal that "there's no waiting reader".
      semaphore_t sem_waiting_reader;
    } pw;
  } d;
} rwlock_t;

/**
 * @brief Initialize an rwlock.
 * @param self: Pointer to the rwlock which needs to be initialized.
 * @param type: RWLOCK_READER_PREFERRED or RWLOCK_WRITER_PREFERRED.
 * @return Whether the rwlock is created successfully or not.
 */
bool rwlock_init(rwlock_t *self, rwlock_type type);

/**
 * @brief Destroy an rwlock.
 * @param self: Pointer to the rwlock which needs to be destroyed.
 * @note: This method would not check whether there are active or waiting 
 *        readers and writers, so use it carefully.
 */
void rwlock_destroy(rwlock_t *self);

/**
 * @brief Try to acquire the reader of an rwlock.
 * @param self: Pointer to the rwlock instance.
 */
void rwlock_lock_read(rwlock_t *self);

/**
 * @brief Release the reader of an rwlock.
 * @param self: Pointer to the rwlock instance.
 */
void rwlock_unlock_read(rwlock_t *self);

/**
 * @brief Try to acquire the writer of an rwlock.
 * @param self: Pointer to the rwlock instance.
 */
void rwlock_lock_write(rwlock_t *self);

/**
 * @brief Release the writer of an rwlock.
 * @param self: Pointer to the rwlock instance.
 */
void rwlock_unlock_write(rwlock_t *self);

// Check whether rwlock works properly.
void rwlock_check();

#endif // _KERN_SYNC_RWLOCK_H
