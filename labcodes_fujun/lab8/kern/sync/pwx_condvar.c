// @file: kern/sync/pwx_condvar.c
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-06-04

#include <pwx_condvar.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <proc.h>

void pwx_condvar_init(pwx_condvar_t *self)
{
  self->count = 0;
  sem_init(&(self->sem_wait), 0);
  sem_init(&(self->sem_count), 1);
}

void pwx_condvar_wait(pwx_condvar_t *self, semaphore_t *mutex)
{
  // Increase the waiting count and enter into wait.
  down(&(self->sem_count));
    ++self->count;
  up(&(self->sem_count));
  up(mutex); // TODO: Should this be before down(&(self->sem_count))?
  // Wait for signal.
  down(&(self->sem_wait));
  // Re-grab the mutex and decrease the waiting count.
  down(mutex);
}

void pwx_condvar_signal(pwx_condvar_t *self)
{
  down(&(self->sem_count));
  if (self->count > 0) {
    --self->count;
    up(&(self->sem_wait));
  }
  up(&(self->sem_count));
}

void pwx_condvar_broadcast(pwx_condvar_t *self)
{
  down(&(self->sem_count));
  while (self->count > 0) {
    --self->count;
    up(&(self->sem_wait));
  }
  up(&(self->sem_count));
}

// ---- Implement rwlock via condvar ----

typedef struct {
  volatile int active_reader;
  volatile int active_writer;
  volatile int waiting_reader;
  volatile int waiting_writer;
  pwx_condvar_t condvar_can_read;
  pwx_condvar_t condvar_can_write;
  semaphore_t mutex;
}
condvar_rwlock_t;

// Initalize the condvar-based rwlock.
static void condvar_rwlock_init(condvar_rwlock_t *self)
{
  self->active_reader = self->active_writer = 
    self->waiting_reader = self->waiting_writer = 0;
  pwx_condvar_init(&(self->condvar_can_read));
  pwx_condvar_init(&(self->condvar_can_write));
  sem_init(&(self->mutex), 1);
}

// ---- rwlock that prefers writers ----

// lock_read
static void condvar_rwlock_lock_read(condvar_rwlock_t *self)
{
  down(&(self->mutex));
  while (self->active_writer + self->waiting_writer > 0) {
    ++self->waiting_reader;
    pwx_condvar_wait(&(self->condvar_can_read), &(self->mutex));
    --self->waiting_reader;
  }
  ++self->active_reader;
  up(&(self->mutex));
}

// unlock_read
static void condvar_rwlock_unlock_read(condvar_rwlock_t *self)
{
  down(&(self->mutex));
  --self->active_reader;
  if (self->active_reader == 0 && self->waiting_writer > 0) {
    pwx_condvar_signal(&(self->condvar_can_write));
  }
  up(&(self->mutex));

  // Special hack: to ensure writers have the chance to grab rwlock.
  if (current) {
    current->need_resched = 1;
    schedule();
  }
}

// lock_write
static void condvar_rwlock_lock_write(condvar_rwlock_t *self)
{
  down(&(self->mutex));
  while (self->active_reader + self->active_writer > 0) {
    ++self->waiting_writer;
    pwx_condvar_wait(&(self->condvar_can_write), &(self->mutex));
    --self->waiting_writer;
  }
  ++self->active_writer;
  up(&(self->mutex));
}

// unlock_write
static void condvar_rwlock_unlock_write(condvar_rwlock_t *self)
{
  down(&(self->mutex));
  --self->active_writer;
  if (self->waiting_writer > 0) {
    pwx_condvar_signal(&(self->condvar_can_write));
  }
  else if (self->waiting_reader > 0) {
    pwx_condvar_broadcast(&(self->condvar_can_read));
  }
  up(&(self->mutex));

  // Special hack: to ensure other writers have the chance to grab rwlock.
  if (current) {
    current->need_resched = 1;
    schedule();
  }
}

// ---- Test rwlock via condvar ----
#define N 10
#define TIMES 5
#define SLEEP_TIME 5

static condvar_rwlock_t rwlock;

static void rwlock_check_report_state(int name, bool is_read, int loop)
{
  const char* reader_or_writer = (is_read) ? ("reader") : ("writer");
  if (loop >= 0) {
    cprintf("[condvar_rwlock] #%d %s loop %d, active_reader %d, waiting_reader %d, "
            "active_writer %d, waiting_writer %d.\n", 
      name, reader_or_writer, loop, rwlock.active_reader, rwlock.waiting_reader, 
      rwlock.active_writer, rwlock.waiting_writer);
  } else {
    cprintf("[condvar_rwlock] #%d %s exit, active_reader %d, waiting_reader %d, "
            "active_writer %d, waiting_writer %d.\n", 
      name, reader_or_writer, rwlock.active_reader, rwlock.waiting_reader, 
      rwlock.active_writer, rwlock.waiting_writer);
  }
}

static int rwlock_condvar_check_read(void* arg)
{
  int name = (int)arg;
  int iter = 0;
  for (iter = 0; iter < TIMES; iter++) {
    condvar_rwlock_lock_read(&rwlock);
    do_sleep(SLEEP_TIME);
    rwlock_check_report_state(name, 1, iter);
    condvar_rwlock_unlock_read(&rwlock);
  }
  rwlock_check_report_state(name, 1, -1);
  return 0;
}

static int rwlock_condvar_check_write(void *arg)
{
  int name = (int)arg;
  int iter = 0;
  for (iter = 0; iter < TIMES; iter++) {
    condvar_rwlock_lock_write(&rwlock);
    do_sleep(SLEEP_TIME);
    rwlock_check_report_state(name, 0, iter);
    condvar_rwlock_unlock_write(&rwlock);
  }
  rwlock_check_report_state(name, 0, -1);
  return 0;
}

// ---- Test rwlock on condvar ----
void rwlock_check_condvar()
{
  int i, pid;

  condvar_rwlock_init(&rwlock);

  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_condvar_check_read, (void*)i, 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_condvar_check_read_pw");
  }

  for(i=0; i<N/2; i++){
    assert ((pid = kernel_thread(rwlock_condvar_check_write, (void*)i, 0)) != 0);
    set_proc_name(find_proc(pid), "rwlock_condvar_check_write_pw");
  }
}
