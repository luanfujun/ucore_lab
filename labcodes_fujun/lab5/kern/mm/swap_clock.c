// @file: kern/mm/swap_clock.c
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-05-02

#include <swap_clock.h>

#include <list.h>
#include <vmm.h>
#include <error.h>
#include <config.h>

#if defined(USE_MM_EXT) && 0

// The advanced clock replacement algorithm place all allocated pages in a 
// circular linked-list.
struct swap_clock_instance_s {
	list_entry_t pages;
	list_entry_t *next;  // The next page to be checked.
};
typedef struct swap_clock_instance_s swap_clock_instance_t;

// The main swap manager.
swap_clock_instance_t swap_clock_instance;

// Initialize the clock replacement algorithm.
static int 
swap_clock_init(void) 
{
	return 0;
}

// Initialize the swap facility for particular mm structure.
static int
swap_clock_init_mm(struct mm_struct *mm)
{
	list_init(&(swap_clock_instance.pages));
	swap_clock_instance.next = &(swap_clock_instance.pages);
	mm->sm_priv = &swap_clock_instance;
	return 0;
}

// Nofity the swap manager that a page is ready in the memory.
static int
swap_clock_map_swappable(struct mm_struct *mm, uintptr_t addr, struct Page *page, int swap_in)
{
    swap_clock_instance_t *inst = (swap_clock_instance_t*) mm->sm_priv;
    list_entry_t *entry = &(page->page_link);

	// Put the page into the clock list.
	list_add_before(&(inst->pages), entry);
	if (inst->next == &(inst->pages)) {
		inst->next = entry;
	}
	SetPageSwap(page);

	return 0;
}

// Find a suitable page to be swapped out.
static int
swap_clock_swap_out_victim(struct mm_struct *mm, struct Page ** ptr_page, int in_tick)
{
	int exitcode = 0;
    swap_clock_instance_t *inst = (swap_clock_instance_t*) mm->sm_priv;
	list_entry_t *next = inst->next;
	assert(inst != NULL);
	assert(in_tick == 0);

	if (&(inst->pages) == next) {
		exitcode = -E_NO_MEM;
		goto finished;
	}


	for (;; next = list_next(next)) {
		if (next == &(inst->pages)) 
			continue;
		struct Page* page = le2page(next, page_link);
		assert(PageSwap(page));  // Make sure that the page is managed!

		if (PageDirty(page)) {
			ClearPageDirty(page);
		} else if (PageUsed(page)) {
			ClearPageUsed(page);
		} else {
			// Neither dirty nor used, this is exactly the page we want.
			inst->next = list_next(next);
			if (inst->next == &(inst->pages)) {
				// The next page maybe still head of list, which means no more 
				// managed swap pages in clock.
				inst->next = list_next(inst->next); 
			}
			list_del(next);
			*ptr_page = page;
			goto finished;
		}
	}

finished:
    return exitcode;
}

// Notify the swap manager that a page is not swappable.
static int
swap_clock_set_unswappable(struct mm_struct *mm, uintptr_t addr)
{
	// Korepwx: just now ignore this method.
    return 0;
}

// Tick event, may cause the swap manager to swap out pages automatically.
static int 
swap_clock_tick_event(struct mm_struct *mm)
{
	// Korepwx: not swap out page automatically.
	return 0;
}

// Check whether the swap manager works properly
static int
swap_clock_check_swap(void)
{
	// TODO: implement the swap manager test.
	return 0;
}

// Register all functions to the swap manager.
struct swap_manager swap_manager_clock =
{
     .name            = "clock swap manager",
     .init            = &swap_clock_init,
     .init_mm         = &swap_clock_init_mm,
     .tick_event      = &swap_clock_tick_event,
     .map_swappable   = &swap_clock_map_swappable,
     .set_unswappable = &swap_clock_set_unswappable,
     .swap_out_victim = &swap_clock_swap_out_victim,
     .check_swap      = &swap_clock_check_swap,
};

#endif  // USE_MM_EXT
