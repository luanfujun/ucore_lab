// @file: kern/mm/buddy_pmm.c
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Contributors:
//   Korepwx  <public@korepwx.com>  2013-04-19

#include <pmm.h>
#include <list.h>
#include <string.h>
#include <stdio.h>
#include <buddy_pmm.h>
#include <bitops.h>

// Buddy system manages page order from 1 to 10
// which, 4K ~ 4M in memory size.
#define BUDDY_MAX_ORDER 10
#define BUDDY_MAX_SIZE (1 << BUDDY_MAX_ORDER)
static free_area_t buddy_free_area[BUDDY_MAX_ORDER + 1];

// When try to free the memory, relative offset of the page in memory chunk
// is needed to determine whether the page should be combined.
#define BUDDY_MAX_ZONE 10
struct Zone {
	struct Page *page_base;
};
static struct Zone buddy_zones[BUDDY_MAX_ZONE] = { {0} };

// Shortcut to buddy_free_area[x] properties.
#define free_list(x) (buddy_free_area[x].free_list)
#define nr_free(x) (buddy_free_area[x].nr_free)

// Get 2^(n-1) < X <= 2^n
static inline size_t buddy_getorder(size_t n) {
	if (n < BUDDY_MAX_SIZE)
		return (n > 1) ? ufls(n - 1) : 0;
	panic("%u is too large a size for page allocation.\n", n);
}

// Helper function to find out the page index in its belonging zone.
static inline size_t buddy_page2index(struct Page *page) {
	return page - (buddy_zones[page->zone].page_base);
}

// Helper function to get the page at index.
static inline struct Page* buddy_index2page(int zone, size_t idx) {
	return buddy_zones[zone].page_base + idx;
}

// Check whether the page is in particular zone and has the desired size.
static inline bool buddy_check_pair(struct Page *page, int zone, size_t order) {
	if (page2ppn(page) >= npage) // check whether page is valid.
		return 0;
	return page->zone == zone && !PageReserved(page) 
	 	&& PageProperty(page) && page->property == order;
}

// Print the details of free chunks.
/*static void buddy_print_free(void) {
	size_t order;
	for (order = 0; order <= BUDDY_MAX_ORDER; ++order) {
		list_entry_t *head = &(free_list(order)), *le;
		le = list_next(head);
		cprintf("Buddy Memory (order=%d, size=0x%x): %d free\n", 
				order, (1 << order), nr_free(order));
		for (; le != head; le = list_next(le)) {
			struct Page* page = le2page(le, page_link);
			cprintf("\tBuddy Chunk (idx=%d, order=%d): 0x%08x\n", 
				buddy_page2index(page), page->property, page2pa(page));
		}
	}
}*/

// Initialize the buddy system.
static void buddy_init(void) {
	size_t i = 0;
	for (; i <= BUDDY_MAX_ORDER; ++i) {
		nr_free(i) = 0;
		list_init(&free_list(i));
	}
}

// Fit available physical memory into buddy system.
static void buddy_init_memmap(struct Page *base, size_t n) {
	static int zone = 0;
	struct Page *p = base, *pend = base + n;

	for (; p != pend; ++p) {
		// At first, pmm.c:init_page marks every page to be reserved.
		assert(PageReserved(p));
		// Zone is used by alloc & free method.
		p->zone = zone;
		// Clear all page flags for initialize.
		p->flags = p->property = 0;
		set_page_ref(p, 0);
	}
	buddy_zones[zone++].page_base = base;

	size_t order = BUDDY_MAX_ORDER, order_size = (1 << order);
	p = base;
	while (n != 0) {
		// Try to fit 2^10, 2^9, ..., 2^0 size of memory into the table.
		while (n >= order_size) {
			p->property = order;
			SetPageProperty(p);
			list_add(&free_list(order), &(p->page_link));
			n -= order_size; p += order_size;
			++nr_free(order);
		}

		// Ready to fit next order_size.
		--order;
		order_size >>= 1;
	}
}

// Allocate desired order of pages from buddy system.
static struct Page* buddy_alloc_page_block(size_t order) {
	size_t next_order = order;
	for (; next_order <= BUDDY_MAX_ORDER; ++next_order) {
		if (!list_empty(&free_list(next_order))) {
			// Found an available page block.
			list_entry_t *le = list_next(&free_list(next_order));
			struct Page* page = le2page(le, page_link);
			--nr_free(next_order);
			list_del(le);

			// Split the block and give back remaining.
			size_t order_size = 1 << next_order;
			while (next_order > order) {
				--next_order;
				order_size >>= 1;
				struct Page* pair = page + order_size;
				pair->property = next_order;
				SetPageProperty(pair);
				++nr_free(next_order);
				list_add(&free_list(next_order), &(pair->page_link));
			}

			ClearPageProperty(page);
			page->property = order;
			return page;
		}
	}

	return NULL;
}

// Allocate desired size of pages from buddy system.
static struct Page* buddy_alloc_pages(size_t n) {
	assert (n > 0);
	size_t order = buddy_getorder(n);
	size_t order_size = (1 << order);

	struct Page* page = buddy_alloc_page_block(order);
	if (page != NULL && n != order_size) {
		free_pages(page + n, order_size - n);
	}
	return page;
}

// Try to merge one page block into buddy system.
static void buddy_free_page_block(struct Page *base, size_t order) {
	size_t idx = buddy_page2index(base), pair_idx, size = (1 << order);
	assert ((idx & (size - 1)) == 0);

	// Check the page status within this block.
	struct Page *p = base, *pend = base + size;
	for (; p != pend; ++p) {
		assert (!PageReserved(p) && !PageProperty(p));
		p->flags = 0;
		set_page_ref(p, 0);
	}

	// Now let's try to merge every possible block.
	int zone = base->zone;
	struct Page *pair;
	while (order < BUDDY_MAX_ORDER) {
		pair_idx = idx ^ size; // Reverse the index bit.
		pair = buddy_index2page(zone, pair_idx);

		if (!buddy_check_pair(pair, zone, order)) 
			break;

		--nr_free(order);
		list_del(&(pair->page_link));
		ClearPageProperty(pair);
		pair->property = 0;

		idx &= pair_idx; // Clear the last index bit.
		++order; size <<= 1;
	}

	// Put the merged block into buddy_zones.
	p = buddy_index2page(zone, idx);
	SetPageProperty(p);
	p->property = order;
	list_add(&free_list(order), &(p->page_link));
	++nr_free(order);
}

// Free particular size of pages and put back to buddy sytem.
static void buddy_free_pages(struct Page *base, size_t n) {
	assert (n != 0);

	if (n == 1) {
		buddy_free_page_block(base, 0);
	} else {
		size_t order = 0, order_size = 1;

		// Free pages from start address to aligned pair.
		while (n >= order_size) {
			assert (order <= BUDDY_MAX_ORDER);
			if ((buddy_page2index(base) & order_size) != 0) {
				buddy_free_page_block(base, order);
				n -= order_size;
				base += order_size;
			}
			order_size <<= 1;
			++order;
		}

		// Free pages from aligned pair to end.
		while (n != 0) {
			while (n < order_size) {
				order_size >>= 1;
				--order;
			}
			buddy_free_page_block(base, order);
			base += order_size;
			n -= order_size;
		}
	}
}

// Get the maximum free memory count in buddy system.
static size_t buddy_nr_free_pages(void) {
	size_t ret = 0, i = 0;
	for (; i <= BUDDY_MAX_ORDER; ++i) {
		ret += nr_free(i) * (1 << i);
	}
	return ret;
}

// Check the correctness in buddy system.
// NOTE: this function is snipped from uCore.
static void buddy_check(void) {
	int i;
	int count = 0, total = 0;
	for (i = 0; i <= BUDDY_MAX_ORDER; i++) {
		list_entry_t *list = &free_list(i), *le = list;
		while ((le = list_next(le)) != list) {
			struct Page *p = le2page(le, page_link);
			assert(PageProperty(p) && p->property == i);
			count++, total += (1 << i);
		}
	}
	assert(total == nr_free_pages());

	struct Page *p0 = alloc_pages(8);
	struct Page *buddy = alloc_pages(8), *p1;

	assert(p0 != NULL);
	assert(!PageProperty(p0));

	list_entry_t free_lists_store[BUDDY_MAX_ORDER + 1];
	unsigned int nr_free_store[BUDDY_MAX_ORDER + 1];

	for (i = 0; i <= BUDDY_MAX_ORDER; i++) {
		free_lists_store[i] = free_list(i);
		list_init(&free_list(i));
		assert(list_empty(&free_list(i)));
		nr_free_store[i] = nr_free(i);
		nr_free(i) = 0;
	}

	assert(nr_free_pages() == 0);
	assert(alloc_page() == NULL);
	free_pages(p0, 8);
	assert(nr_free_pages() == 8);
	assert(PageProperty(p0) && p0->property == 3);
	assert((p0 = alloc_pages(6)) != NULL && !PageProperty(p0)
	       && nr_free_pages() == 2);

	assert((p1 = alloc_pages(2)) != NULL && p1 == p0 + 6);
	assert(nr_free_pages() == 0);

	free_pages(p0, 3);
	assert(PageProperty(p0) && p0->property == 1);
	assert(PageProperty(p0 + 2) && p0[2].property == 0);

	free_pages(p0 + 3, 3);
	free_pages(p1, 2);
	assert(PageProperty(p0) && p0->property == 3);

	assert((p0 = alloc_pages(6)) != NULL);
	assert((p1 = alloc_pages(2)) != NULL);
	free_pages(p0 + 4, 2);
	free_pages(p1, 2);

	p1 = p0 + 4;
	assert(PageProperty(p1) && p1->property == 2);
	free_pages(p0, 4);
	assert(PageProperty(p0) && p0->property == 3);

	assert((p0 = alloc_pages(8)) != NULL);
	assert(alloc_page() == NULL && nr_free_pages() == 0);

	for (i = 0; i <= BUDDY_MAX_ORDER; i++) {
		free_list(i) = free_lists_store[i];
		nr_free(i) = nr_free_store[i];
	}

	free_pages(p0, 8);
	free_pages(buddy, 8);

	assert(total == nr_free_pages());

	for (i = 0; i <= BUDDY_MAX_ORDER; i++) {
		list_entry_t *list = &free_list(i), *le = list;
		while ((le = list_next(le)) != list) {
			struct Page *p = le2page(le, page_link);
			assert(PageProperty(p) && p->property == i);
			count--, total -= (1 << i);
		}
	}
	assert(count == 0);
	assert(total == 0);
}

// Sometimes it is necessary to backup pmm states for test purpose.
static free_area_t buddy_free_area_clone[BUDDY_MAX_ORDER + 1];
static struct Zone buddy_zones_clone[BUDDY_MAX_ZONE];

void buddy_backup_states() {
	int i;
	for (i=0; i<=BUDDY_MAX_ORDER; ++i)
		buddy_free_area_clone[i] = buddy_free_area[i];
	for (i=0; i<BUDDY_MAX_ZONE; ++i)
		buddy_zones_clone[i] = buddy_zones[i];
}
void buddy_restore_states() {
	int i;
	for (i=0; i<=BUDDY_MAX_ORDER; ++i)
		buddy_free_area[i] = buddy_free_area_clone[i];
	for (i=0; i<BUDDY_MAX_ZONE; ++i)
		buddy_zones[i] = buddy_zones_clone[i];
}

// Expose all methods of buddy pmm manager.
const struct pmm_manager buddy_pmm_manager = {
    .name = "buddy_pmm_manager",
    .init = buddy_init,
    .init_memmap = buddy_init_memmap,
    .alloc_pages = buddy_alloc_pages,
    .free_pages = buddy_free_pages,
    .nr_free_pages = buddy_nr_free_pages,
    .check = buddy_check,
};


